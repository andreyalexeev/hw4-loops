"use strict";

// task 1

let num1;
let num2;

do {
  num1 = prompt("Введіть перше число:");
} while (isNaN(num1));

do {
  num2 = prompt("Введіть друге число:");
} while (isNaN(num2));

num1 = parseFloat(num1);
num2 = parseFloat(num2);

let minNum = Math.min(num1, num2);
let maxNum = Math.max(num1, num2);

for (let i = Math.ceil(minNum); i <= maxNum; i++) {
  alert(i);
}

// task 2

// let userInput;
// let isEven = false;

// do {
//   userInput = prompt("Введіть будь-яке число:");

//   if (isNaN(userInput)) {
//     alert("Введено не число. Будь ласка, введіть число.");
//   } else {

//     let number = parseInt(userInput);

//     if (number % 2 === 0) {
//       isEven = true;
//       alert("Ви ввели парне число.");
//     } else {
//       alert("Ви ввели непарне число. Спробуйте ще раз.");
//     }
//   }
// } while (!isEven);

